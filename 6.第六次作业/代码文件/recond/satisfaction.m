function output = satisfaction(data)
%此函数用于求出满意度
The_MAx=max(max(data));
The_Min=min(min(data));
Data_Size=size(data);
%生成一个与data行列相同的全0矩阵
output=zeros(size(data));
for i=1:Data_Size(1)
    for j=1:Data_Size(2)
    output(i,j)=(data(i,j)-The_Min)/(The_MAx-The_Min);
    end
end
end

