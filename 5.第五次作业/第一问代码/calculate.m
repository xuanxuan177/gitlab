function output= calculate(data,data_chance)
%用于计算1、2、3等奖的金额
%其中
    %输出：output为一个1乘3的行矩阵
    %输入：data前七列（1到7等奖的比例or金额）
    %data_chance前七列（概率）
    sum1=sum(data(4:7).*data_chance(4:7));
    output=(1-sum1).*data(1:3)./data_chance(1:3);
end

