%此程序用于解决第一问：
    %1。读取题目中给的数据：data。
    %2.通过不同的概率计算方法处理data，生成data_final
%1.读取、转换格式
clear
load('data.mat');
data=table2array(data); %将table格式转化为矩阵
data_chance=data;
data_chance(23,6)=0;
data_chance(23,7)=0;
%2.计算概率
%3.计算一二三等奖的金额
for i=1:29
    switch data(i,8)
        case 1
            data_chance(i,1)=2*10^(-7);
            data_chance(i,2)=8*10^(-7);
            data_chance(i,3)=1.8*10^(-5);
            data_chance(i,4)=2.61*10^(-4);
            data_chance(i,5)=3.42*10^(-3);
            data_chance(i,6)=4.2039*10^(-2);
        case 2
            data_chance(i,1:8)=K2(data_chance(i,10),data_chance(i,9));
        case 3
            data_chance(i,1:8)=K3(data_chance(i,10),data_chance(i,9));
        case 4
            data_chance(i,1:8)=K4(data_chance(i,10),data_chance(i,9));
    end
end
%计算1、2、3等奖的金额，并计入全部数据的矩阵
money_data=data(:,1:7);
for i=1:29
    money_data(i,1:3)=calculate(data(i,1:7),data_chance(i,1:7));
end
%4.计算F
data_F=money_data(:,1:3);
for i=1:29
    data_F(i,4)=F(money_data(i,1:7),data_chance(i,1:7));
end
data_F(23,4)=0;









